// block tweets and/or report users on twitter
package main

import (
	"bufio"
	"encoding/gob"
	"log"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/ChimeraCoder/anaconda"
)

const (
	BLOCKEDFILENAME = ".blocked.gob"  // default filename of cached blocked list
	BLOCKEDFRESH    = time.Hour       // consider the cache file fresh if it was written out under this duration
	SLEEP           = 5 * time.Minute // sleep for this long before fetching the mentions list
	BLOCK           = "block"
	REPORT          = "report"
)

type Block struct {
	action string
	match  string
}

type BlockedSerialized struct {
	LastFetch    time.Time
	BlockedUsers []string
}

var (
	api          *anaconda.TwitterApi
	blocks       []Block
	blockedusers map[string]bool = make(map[string]bool)
)

func main() {
	log.SetFlags(log.Ldate | log.Ltime | log.LUTC | log.Lmicroseconds)
	log.Println("starting...")
	readblocklist()
	twittersetup()
	getblockedusers()
	fetchandprocess()
}

func readblocklist() {
	var parts []string
	b := Block{}

	filename := os.Getenv("BLOCKFILE")

	if filename == "" {
		log.Fatalf("missing env var: BLOCKFILE")
	}

	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		parts = strings.SplitAfter(scanner.Text(), ":")
		// skip empty lines, comments
		if len(parts) == 0 || parts[0] == "" {
			continue
		}
		if strings.HasPrefix(parts[0], "#") || strings.HasPrefix(parts[0], "//") {
			continue
		}

		b.match = strings.ToLower(parts[1])
		if parts[0] == "block:" {
			b.action = BLOCK
			log.Printf("block: %s", parts[1])
		} else if parts[0] == "report:" {
			b.action = REPORT
			log.Printf("report: %s", parts[1])
		}
		blocks = append(blocks, b)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	if len(blocks) == 0 {
		log.Fatal("no blocks, exiting.")
	}
}

func twittersetup() {
	apikey := os.Getenv("TWITTERKEY")
	apisecret := os.Getenv("TWITTERSECRET")
	accesstoken := os.Getenv("TWITTERACCESS")
	accesssecret := os.Getenv("TWITTERACCESSSECRET")

	if apikey == "" || apisecret == "" || accesstoken == "" || accesssecret == "" {
		log.Fatalln("missing env vars: TWITTERKEY, TWITTERSECRET, TWITTERACCESS, TWITTERACCESSSECRET")
	}

	anaconda.SetConsumerKey(apikey)
	anaconda.SetConsumerSecret(apisecret)
	api = anaconda.NewTwitterApi(accesstoken, accesssecret)
	api.ReturnRateLimitError(true)
}

func getblockedusers() {
	// for reading/writing serialized files
	bs := BlockedSerialized{}

	var u anaconda.User
	var uc anaconda.UserCursor
	var err error
	var next int64
	var nextstr string
	vals := url.Values{}

	// try to load a saved file
	filename := os.Getenv("BLOCKEDCACHE")
	if filename == "" {
		filename = BLOCKEDFILENAME
	}
	file, err := os.Open(filename)
	if err == nil {
		defer file.Close()

		dec := gob.NewDecoder(file)
		err = dec.Decode(&bs)
		if err == nil {
			log.Printf("loaded cached blocked users list, updated: %s users: %d",
				bs.LastFetch.Format(time.RFC3339), len(bs.BlockedUsers))
			if time.Now().Sub(bs.LastFetch) < BLOCKEDFRESH {
				for _, u := range bs.BlockedUsers {
					blockedusers[u] = true
				}
				return
			} else {
				log.Println("cached list is stale, fetching new list")
			}
		}
	}

	vals.Set("include_entities", "false")
	vals.Set("skip_status", "true")
	for {
		if next > 0 {
			vals.Set("cursor", nextstr)
		}
		<-time.After(time.Minute)
		uc, err = api.GetBlocksList(vals)
		if err != nil {
			handleratelimit(err, "get-blocks")
			uc, err = api.GetBlocksList(vals)
		}
		// no blocked users
		if len(uc.Users) == 0 {
			return
		}
		for _, u = range uc.Users {
			bs.BlockedUsers = append(bs.BlockedUsers, u.ScreenName)
			blockedusers[u.ScreenName] = true
		}
		log.Printf("got blocked users: %d; next cursor: %d", len(uc.Users), uc.Next_cursor)
		if uc.Next_cursor == 0 || uc.Next_cursor == next {
			break
		} else {
			next = uc.Next_cursor
			nextstr = uc.Next_cursor_str
		}
	}
	log.Printf("blocked users: %d", len(blockedusers))

	file, err = os.Create(BLOCKEDFILENAME)
	if err != nil {
		log.Printf("failed to create %s to save blocked users: %s", BLOCKEDFILENAME, err)
		return
	}
	defer file.Close()

	bs.LastFetch = time.Now()
	enc := gob.NewEncoder(file)
	err = enc.Encode(bs)
	if err != nil {
		log.Print(err)
		return
	}
}

func fetchandprocess() {
	var tweet anaconda.Tweet
	var tweets []anaconda.Tweet
	var err error
	var id int64
	var idstr string
	var count int
	vals := url.Values{}

	vals.Set("count", "200")
	vals.Set("include_entities", "false")

	for {
		count = 0
		if id > 0 {
			vals.Set("since_id", idstr)
		}
		tweets, err = api.GetMentionsTimeline(vals)
		if err != nil {
			handleratelimit(err, "mentions")
			tweets, err = api.GetMentionsTimeline(vals)
		}

		log.Printf("got tweets: %d", len(tweets))
		for _, tweet = range tweets {
			if tweet.Id > id {
				id = tweet.Id
				idstr = tweet.IdStr
			}
			count += process(tweet)

		}
		log.Printf("blocked/reported: %d", count)
		<-time.After(SLEEP)
	}
}

func process(tweet anaconda.Tweet) int {
	var already bool

	for _, block := range blocks {
		if strings.Contains(strings.ToLower(tweet.Text), block.match) {
			_, already = blockedusers[tweet.User.ScreenName]
			if already {
				log.Printf("skipping %s; already blocked", tweet.User.ScreenName)
			} else {
				if block.action == BLOCK {
					doblock(tweet)
					return 1
				} else if block.action == REPORT {
					doreport(tweet)
					doblock(tweet)
					return 1
				}
			}
		}
	}
	return 0
}

func doblock(tweet anaconda.Tweet) {
	_, err := api.BlockUser(tweet.User.ScreenName, nil)
	if err != nil {
		handleratelimit(err, "block")
		doblock(tweet)
	}
	log.Printf("%s blocked", tweet.User.ScreenName)
	blockedusers[tweet.User.ScreenName] = true
}

func doreport(tweet anaconda.Tweet) {
	_, err := api.ReportSpam(tweet.User.ScreenName, nil)
	if err != nil {
		handleratelimit(err, "report")
		doreport(tweet)
	}

	log.Printf("%s reported for %s", tweet.User.ScreenName, tweet.Text)
}

func handleratelimit(err error, call string) {
	if aerr, ok := err.(*anaconda.ApiError); ok {
		if isRateLimitError, nextWindow := aerr.RateLimitCheck(); isRateLimitError {
			log.Printf("%s rate-limited; trying after %v", call, nextWindow.Sub(time.Now()))
			<-time.After(nextWindow.Sub(time.Now()))
		}
	} else {
		log.Fatal(err)
	}
}
