I am tired of people spamming me on twitter.  This autoblocks twitter
users and/or autoreports tweets.  It's a work in progress.

These environment variables need to be set:

    TWITTERKEY                  Consumer Key (API Key)
    TWITTERSECRET               Consumer Secret (API Secret)
    TWITTERACCESS               Access Token
    TWITTERACCESSSECRET         Access Token Secret
    BLOCKFILE                   Path to a file with rules (see below)
    BLOCKEDCACHE                Path to the file with a cached list of blocked users (optional)


The `BLOCKEDCACHE` environment variable, if not set, defaults to
`.blocked.gob` in the current directory.

The `BLOCKFILE` has a list of rules to either block users or report
tweets (and also block the users who wrote them).  At present, the
code uses a simple substring match of the lower-cased text.  The file
looks like:

    # comment, must be the first character of a line
    // C++-style single-line comments are also supported
    # pattern: block:some string to match
    block:lol
    block:hate u
    # pattern: report:some string to match
    report:win 5 nights in verona italy

The blocker works like this:

1. read the `BLOCKFILE`
1. set up the twitter api with the api secret, etc.
1. check if we have a list of already blocked users cached.  if the
   cache was written out over an hour ago, consider it stale
1. if we need it, get a list of already blocked users, and save it
1. loop forever:
  - fetch the user's mentions
  - find tweets to block and/or report, and do it
  - sleep for five minutes

This code depends on the patch in
[issue #124](https://github.com/ChimeraCoder/anaconda/issues/124)
against [anaconda](https://github.com/ChimeraCoder/anaconda/).
